/* jshint node:true,mocha:true,esversion:6,expr:true */
'use strict';
const should = require('chai').should();
const bcra = require('../index');
const protocol = require('../lib/protocol.js');

describe('bcra', function() {
    this.slow(300);

    describe('#summary()', function() {
        it('test the summary function', function() {
            return bcra.summary().then(function(data) {
                data.should.be.a('array').with.length(5);

                data[0].should.be.a('object');
                data[0].should.has.property('label').not.empty;
                data[0].should.has.property('date').not.empty;
                data[0].should.has.property('reference').and.is.greaterThan(0);

                data[1].should.be.a('object');
                data[1].should.has.property('label').not.empty;
                data[1].should.has.property('buy').and.is.greaterThan(0);
                data[1].should.has.property('sale').and.is.greaterThan(0);

                data[2].should.be.a('object');
                data[2].should.has.property('label').not.empty;
                data[2].should.has.property('time').not.empty;
                data[2].should.has.property('buy').and.is.greaterThan(0);
                data[2].should.has.property('sale').and.is.greaterThan(0);

                data[3].should.be.a('object');
                data[3].should.has.property('label').not.empty;
                data[3].should.has.property('time').not.empty;
                data[3].should.has.property('buy').and.is.greaterThan(0);
                data[3].should.has.property('sale').and.is.greaterThan(0);

                data[4].should.be.a('object');
                data[4].should.has.property('label').not.empty;
                data[4].should.has.property('time').not.empty;
                data[4].should.has.property('buy').and.is.greaterThan(0);
                data[4].should.has.property('sale').and.is.greaterThan(0);
            });
        });
    });

    describe('#uvi()', function() {
        it('test the uvi', function() {
            return bcra.uvi().then(function(value) {
                value.should.be.a("number").and.is.greaterThan(0);
            });
        });
    });
});
