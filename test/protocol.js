/* jshint node:true,mocha:true,esversion:6,expr:true */
'use strict';
const should = require('chai').should();
const protocol = require('../lib/protocol.js');

describe('protocol', function() {
    describe('#parseSummaryItem()', function() {
        it('parse valid text with reference cotization', function() {
            const text = 'DOLAR DE REFERENCIA 3/8/2016: $ 14,8675.';
            var data = protocol.parseSummaryItem(text);
            data.should.be.a('object');
            data.should.has.property('label').equal('DOLAR DE REFERENCIA');
            data.should.has.property('date').equal('3/8/2016');
            data.should.has.property('reference').equal(14.8675);
        });

        it('parse valid text without date', function() {
            const text = 'DOLAR MAYORISTA, CIERRE: $ 14,8600/ 14,8800.';
            var data = protocol.parseSummaryItem(text);

            data.should.be.a('object');
            data.should.has.property('label').equal('DOLAR MAYORISTA');
            data.should.has.property('buy').equal(14.86);
            data.should.has.property('sale').equal(14.88);
        });

        it('parse valid text with time', function() {
            const text = 'EURO / DOLAR, A LAS 15:00: 1,1139/ 1,1145';
            var data = protocol.parseSummaryItem(text);

            data.should.be.a('object');
            data.should.has.property('label').equal('EURO / DOLAR');
            data.should.has.property('time').equal('15:00');
            data.should.has.property('buy').equal(1.1139);
            data.should.has.property('sale').equal(1.1145);
        });

        it('parse invalid text', function() {
            const text = 'Jon Snow';
            var data = protocol.parseSummaryItem(text);

            data.should.be.a('object');
            data.should.has.property('label').equal('Jon Snow');
            data.should.has.not.property('time');
            data.should.has.not.property('buy');
            data.should.has.not.property('sale');
        });

        it('parse null', function() {
            const text = null;
            var data = protocol.parseSummaryItem(text);
            should.not.exist(data);
        });

        it('parse undefined', function() {
            var text;
            var data = protocol.parseSummaryItem(text);
            should.not.exist(data);
        });
    });

    describe('#parseValue()', function() {
        it('parse valid text', function() {
            const text = 'u$d 14,8675';
            var data = protocol.parseValue(text);
            data.should.be.a('number').equal(14.8675);
        });

        it('parse invalid text', function() {
            const text = 'no value';
            var data = protocol.parseValue(text);
            data.should.be.a('number').equal(0);
        });
    });
});
