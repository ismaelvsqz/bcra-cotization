/* jshint node:true,mocha:true,esversion:6,expr:true */
'use strict';
const should = require('chai').should();
const bcra = require('../index');
const nock = require('nock');
const resources = require('../lib/resources');

describe('bcra mocked', function() {
    describe('#summary()', function() {
        before(function() {
            nock(resources.URL_BCRA_BASE)
                .get(resources.URL_SUMARY)
                .replyWithError('no data')
                .get(resources.URL_SUMARY)
                .reply(200, 'no xml');
        });

        after(function() {
            nock.cleanAll();
        });

        // Tests
        it('invalid response', function() {
            return bcra.summary().should.be.rejected;
        });

        it('invalid xml', function() {
            return bcra.summary().should.be.rejected;
        });
    });
});
