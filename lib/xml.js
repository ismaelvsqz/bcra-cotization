/* jshint node:true,esversion:6 */
'use strict';
const saxCreateStream = require('sax').createStream;

exports.createStream = function(message) {
    return saxCreateStream(true, {
        trim: true,
        normalize: true,
        lowercase: true
    });
};
