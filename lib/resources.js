/* jshint node:true,esversion:6 */
'use strict';
const BCRA_HOST = 'www.bcra.gob.ar';
const URL_BCRA_BASE = 'http://' + BCRA_HOST;

const ERROR_01 = 'The server status code is: ';
const ERROR_02 = 'The content location is not defined';
const ERROR_03 = 'The message is not a valid XML';
const ERROR_04 = 'The message could not be processed';

exports.BCRA_HOST = BCRA_HOST;
exports.URL_BCRA_BASE = URL_BCRA_BASE;

exports.URL_SUMARY = URL_BCRA_BASE + '/Pdfs/Prensa_comunicacion/info_banner.xml';
exports.URL_INFLACION = URL_BCRA_BASE + '/Pdfs/Prensa_comunicacion/inflacion.xml';
exports.URL_UVI = URL_BCRA_BASE + '/Pdfs/Prensa_comunicacion/UVI.xml';
exports.URL_DOLAR = URL_BCRA_BASE + '/Pdfs/Prensa_comunicacion/dolar.xml';
exports.URL_RESERVAS = URL_BCRA_BASE + '/Pdfs/Prensa_comunicacion/reservas.xml';
exports.URL_REAL = URL_BCRA_BASE + '/Pdfs/Prensa_comunicacion/real.xml';
exports.URL_BM = URL_BCRA_BASE + '/Pdfs/Prensa_comunicacion/BM.xml';
exports.URL_EURO = URL_BCRA_BASE + '/Pdfs/Prensa_comunicacion/euro.xml';

exports.error01 = function(code) {
    return new Error(ERROR_01 + code);
};

exports.error02 = function() {
    return new Error(ERROR_02);
};

exports.error03 = function() {
    return new Error(ERROR_03);
};

exports.error04 = function() {
    return new Error(ERROR_04);
};
