/* jshint node:true,esversion:6 */
'use strict';
const DATE_REGEX = /[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}/;
const TIME_REGEX = /[0-9]{1,2}:[0-9]{1,2}/;
const MONEY_REGEX = /[0-9]+\,[0-9]*/g;

function extractDate(text) {
    var matches = text.match(DATE_REGEX);
    if (!matches) return null;
    return matches[0];
}

function extractTime(text) {
    var matches = text.match(TIME_REGEX);
    if (!matches) return null;
    return matches[0];
}

function extractMonies(text) {
    var matches = text.match(MONEY_REGEX);
    if (!matches) return null;
    return matches;
}

function cleanLabel(text) {
    return text.replace(/[,|\:].*/, '').replace('$', '').trim();
}

function parseMoney(text) {
    return parseFloat(text.replace(',', '.')) || 0;
}

exports.parseValue = function(text) {
    var matches = extractMonies(text);
    if (!matches) return 0;
    return parseMoney(matches[0]);
};

exports.parseSummaryItem = function(text) {
    if (!text)
        return null;

    var item = {};

    var date = extractDate(text);
    if (date) {
        item.date = date;
        text = text.replace(date, '');
    }

    var time = extractTime(text);
    if (time)
        item.time = extractTime(text);

    var matches = extractMonies(text);
    if (matches) {
        if (matches.length == 1) {
            item.reference = parseMoney(matches[0]);
        } else if (matches.length == 2) {
            item.buy = parseMoney(matches[0]);
            item.sale = parseMoney(matches[1]);
        }
    }

    item.label = cleanLabel(text);

    return item;
};
