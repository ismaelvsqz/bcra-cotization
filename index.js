/* jshint node:true,esversion:6 */
'use strict';
const http = require('http');
const q = require('q');
const protocol = require('./lib/protocol');
const resources = require('./lib/resources');
const xml = require('./lib/xml');

exports.summary = function(callback) {
    var deferred = q.defer();

    http.get(resources.URL_SUMARY)
        .on('response', function(message) {
            if (message.statusCode != 200) {
                deferred.reject(resources.error01(message.statusCode));
                return;
            }

            var parser = xml.createStream();
            var dataNode = false;
            var data = [];

            parser.on('error', function(e) {
                deferred.reject(resources.error03());
            });

            parser.on('opentag', function(node) {
                dataNode = (node.name == 'zin');
            });

            parser.on('text', function(node) {
                if (dataNode) {
                    data.push(protocol.parseSummaryItem(node));
                }
            });

            parser.on('closetag', function(name) {
                dataNode = !(name == 'zin');
            });

            parser.on('end', function() {
                if (!data.length) {
                    deferred.reject(resources.error04());
                    return;
                }

                deferred.resolve(data);
            });

            message.pipe(parser);
        });

    deferred.promise.nodeify(callback);
    return deferred.promise;
};

exports.uvi = function(callback) {
    var deferred = q.defer();

    http.get(resources.URL_UVI)
        .on('response', function(message) {
            if (message.statusCode != 200) {
                deferred.reject(resources.error01(message.statusCode));
                return;
            }

            var parser = xml.createStream();
            var dataNode = false;
            var data;

            parser.on('error', function(e) {
                deferred.reject(resources.error03());
            });

            parser.on('opentag', function(node) {
                dataNode = (node.name == 'zin');
            });

            parser.on('text', function(node) {
                if (dataNode) {
                    data = protocol.parseValue(node);
                }
            });

            parser.on('closetag', function(name) {
                dataNode = !(name == 'zin');
            });

            parser.on('end', function() {
                if (data === undefined) {
                    deferred.reject(resources.error04());
                    return;
                }

                deferred.resolve(data);
            });

            message.pipe(parser);
        });

    deferred.promise.nodeify(callback);
    return deferred.promise;
};
